Workshop on Variable Fonts and manual interpolation (with stroke fonts inside) with La Cambre master students in type media, Brussels, 2018. See the main documentation that we have gathered at this occasion on [this blog post](http://blog.osp.kitchen/live/dirty-variables-workshop.html).

![When it's working!](https://gitlab.constantvzw.org/osp/workshop.dirty-variables/raw/673b2cbe180d11473460895eab97ebd16090c662/iceberg/hop.gif "When it's working!")

## Cloning
First clone this repository, when you have no account on the Constant gitlab use:

```
http://gitlab.constantvzw.org/osp/workshop.dirty-variables.git
```
**You'll not be able to push without an account**

If you have an account use:

```
git@gitlab.constantvzw.org:osp/workshop.dirty-variables.git
```

Some example fonts were added as a submodule, retrieve them with the following commands:

```
git submodule init
git submodule update 
```

## Inspector

We are using Fonttools, which ttx format is a clear xml dump of every table present in a (variable) font. See https://github.com/fonttools/fonttools

Our tailor-made inspector web tool displays the content of ttx files, simply "Load the ttx file" then select the glyph to preview in the dropdown menu.